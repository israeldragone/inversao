#!/usr/bin/python3
'''
Programa desenvolvido para inversao e modelagem de FR, sendo possivel criar 
a FR com uma camada mais semi espaco, ou duas camadas mais semi espaco. De forma 
semelhante se pode fazer a inversao de FRs com as mesmas configuracoes descritas.
Alem disso, sao fornecidas funcoes para o calculo dos coeficientes de sensibilidade 
dos paramentros do modelo, com funcoes graficas para sua visualizacao. Tambem e 
calculada a matriz de covariancia dos parametros, desvio padrao dos dados e numero
de condicao do problema
'''
#############################################################
##### Bibliotecas do Python 3 necessarias para execucao   ###
## 1- numpy                                               ###
## 2- scipy                                               ###
## 3- matplotlib                                          ###
## 4- math                                                ###
## 5- os                                                  ###
#############################################################


##importa as bibliotecas
import sys, os, glob, math, operator
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as ss
from scipy.optimize import differential_evolution
import pyswarms as pso
from pyrocko import io, trace


def help():
    '''
    Menu de ajuda
    '''


    os.system('clear') # limpa o terminal
    print("Para executar as funcoes adequadamente siga as seguintes instrucoes:\n\n")
    print("Modelo direto: python fr.py -fwd modelo_camadas arquivo_saida valor_ruido\n")
    print("Modelo inverso: python fr.py -inv arquvio_FR(x,y) n_camadas(2,3) n_invesoes\n")
    print("Calculo das derivadas, parametros de sensibilidade, covariancia e numero de condi\
    cao: python fr.py -sens modelo_camadas\n")
    print("Plots para comparacao do ajuste da inversao: python fr.py -plot arquvio_FR(x,y) \
    modelo_inicial modelo_invertido n_invesoes\n")

    return 0


def plot2(x,y,y2,i,ninv,res,fname):
    '''
    Plota a comparacao da FR real com a calculada
    '''

#   cria a figura
    if (i==1):
        plt.figure(figsize=(10,5))
        plt.plot(x,y2,"--",color="gray",lw=2)

#   na ultima chamada plota a curva real, adiciona legendas e finaliza a figura
    elif (i == ninv):
        plt.plot(x,y,"r",label="Inicial",lw=4)
        plt.plot(x,y2,"--",color="gray",label="Calculado",lw=2 )
        plt.title("residuo medio= "+str(res/ninv))
        plt.xlabel("tempo (s)")
        plt.ylabel("amplitude")
        plt.legend()
#        plt.show()
        plt.savefig("newfigures/"+fname+"_fr.png")

#   plota a curva calculada, chamada entre 1 ate n-1 (n-2 curvas)
    else:

        plt.plot(x,y2,"--",color="gray",lw=2)

    return 0



def plot(data1,data2,data3,i,ninv,fname):
    '''
    plota a comparacao do modelo de velocidade real com o invertido
    '''

    a_h1,a_h2,a_h3,a_vs1,a_vs2,a_vs3 = preparePlot(data1,data2,data3)

    if (i == 1):
        plt.figure(figsize=(5,8))
        plt.gca().invert_yaxis() ##inverte os eixos x e y
        plt.plot(a_vs2,a_h2,"--",color="gray",lw=2)

#   na ultima chamada plota a curva real, adiciona legendas e finaliza a figura
    elif (i == ninv):
        plt.plot(a_vs1,a_h1,"r",label="Inicial",lw=4)
        plt.plot(a_vs3,a_h3,"--",color="blue",label="Medio",lw=3)
        plt.plot(a_vs2,a_h2,"--",color="gray",label="Calculado",lw=2)
        plt.xlabel("velocidade (km/s)")
        plt.ylabel("profundidade")
        plt.legend()
#        plt.show()
        plt.savefig("newfigures/"+fname+"_model.png")

#   plota a curva calculada, chamada entre 1 ate n-1 (n-2 curvas)
    else:
        plt.plot(a_vs2,a_h2,"--",color="gray",lw=2)


    return 0


def preparePlot(data1,data2,data3):
    '''
    Faz algumas manipulacoes nos dados de entrada para facilitar o plot da curva de velocidades por profundidade
    '''

#   le os arquivos e salva cada coluna num vetor
    h1,vp1,vs1,rho1 = np.loadtxt(data1,unpack="True")
    h2,vp2,vs2,rho2 = np.loadtxt(data2,unpack="True")
    h3,vp3,vs3,rho3 = np.loadtxt(data3,unpack="True")

#   cria vetores preenchidos com zeros com o dobro do tamanho do vetor h1
    a_h1=np.zeros(2*len(h1))
    a_h2=np.zeros(2*len(h1))
    a_h3=np.zeros(2*len(h1))
    a_vs1=np.zeros(2*len(h1))
    a_vs2=np.zeros(2*len(h1))
    a_vs3=np.zeros(2*len(h1))

#   inicializa os vetores de profundidade com 0 e de velocidade com o valor de v0
    a_h1[0]=0
    a_h2[0]=0
    a_h3[0]=0
    a_vs1[0]=vs1[0]
    a_vs2[0]=vs2[0]
    a_vs3[0]=vs3[0]

#   manipulacoes para o preenchimento do vetor de velocidades e profundidades para facilitar o plote da curva de velociade por profundidade 

    j=0
    for i in range(1,2*len(vs1)):
        if (i%2==0):
            j+=1
            a_vs1[i]=vs1[j]
            a_vs2[i]=vs2[j]
            a_vs3[i]=vs3[j]
        else:
            a_vs1[i]=vs1[j]
            a_vs2[i]=vs2[j]
            a_vs3[i]=vs3[j]

    j=0
    for i in range(1,2*len(h1)):
        if (i%2==0):
            a_h1[i]=h1[j]+h1[j-1]
            a_h2[i]=h2[j]+h2[j-1]
            a_h3[i]=h3[j]+h3[j-1]
            if (i<(2*len(h1)-2)):
                j+=1

        else:

            a_h1[i]=h1[j]+h1[j-1]
            a_h2[i]=h2[j]+h2[j-1]
            a_h3[i]=h3[j]+h3[j-1]

#   na ultima posicao do vetor de profundidades e um incremento em relacao ao ultimo valor calculado para representar a velocidade no semi espaco

    a_h1[-1]+=a_h1[-1]*0.4
    a_h2[-1]+=a_h2[-1]*0.4
    a_h3[-1]+=a_h3[-1]*0.4

    return (a_h1,a_h2,a_h3,a_vs1,a_vs2,a_vs3)

# faz o arredondamento das variaves, foi escolhido esse metodo para facilmente se alterar a precisao de arredondamento
def round2 (a):

    return (float(format(a,'.1f')))

def round3 (a):

    return (float(format(a,'.3f')))

def normalize(vector):

    aux = np.zeros(len(vector))

    min = np.min(vector)
    max = np.max(vector)

    for i in range(vector.size):
        aux[i] = (vector[i]) / (max-min)

    return aux

# calcula a funcao gaussiana
def gaussian(x, alpha, r):

    g = 1./(math.sqrt(alpha**math.pi))*np.exp(-alpha*np.power((x - r), 2.))
    return (g)


def createSpikes(x,y,delays):
    '''
    Cria os spikes das chegadas das ondas P, S e das conversoes

    truque para evitar problemas na comparacao com float, se por exemplo a onda P chegou em 9.7 s (delays["P"]["tp"] = 9.7) com amplitude 0.5 (delays["P"]["rp"] = 0.5) teremos que:
    y(|x - 9.7| <= 0.01) += 0.5 , ou seja, se |x - 9.7| <= 0.01) -> y(x) = 0.5
    '''
    y[abs(x - delays["P"]["tp"]) <= 0.001] += delays["P"]["rp"]*20

    for i in range(len(delays["S"]["ts"])):
        y[abs(x - delays["S"]["ts"][i]) <= 0.001] += (delays["S"]["rs"][i]*10)/(delays["P"]["rp"]*4)

    for i in range(len(delays["dRef"]["tref"])):
        y[abs(x - delays["dRef"]["tref"][i]) <= 0.001 ]+= (delays["dRef"]["rref"][i]*10)/(delays["P"]["rp"]*6)


    return (y)


def iniModel (filename):
    '''
    le o arquivo de entrada e o retorna na forma de vetor
    '''

    #le o arquivo com valores do modelo
    h,vp,vs,rho = np.loadtxt(filename,unpack="True")

    return (np.concatenate((h,vp,vs,rho)))


def tP (delays, n):
    '''
    Calcula o tempo de chegada da P
    '''

    tp=0
    rp=0
    for i in range(n-1):
        tp+=delays[i]["tp"]
        rp+=delays[i]["rp"]

    dP={"tp":round2(tp),"rp":round2(rp)}

    return (dP)



def timeConversionsS (delays_layers,n):
    '''
    Calcula o tempo de chegada das fases S refratadas
    '''

    delaysS = [0]*(pow(2,n-2))
    rS = [1]*(pow(2,n-2))

    for i in range(0,n-1,1):
        for j in range(pow(2,n-2)):

# ts_n-1
            if (i==(n-2)):
                delaysS[j]=round2(delaysS[j]+delays_layers[n-2]["ts"])
                rS[j]=round2(rS[j]*delays_layers[n-2]["rs"])

            else:
                if (i%2==1):##linhas impares
                    if(j%2==0):
                        delaysS[j]+=delays_layers[i]["tp"]
                        rS[j]*=delays_layers[0]["rp"]
                    else:
                        delaysS[j]+=delays_layers[i]["ts"]
                        rS[j]*=delays_layers[0]["rs"]

                if(i%2==0): ##linhas pares exceto a primeira
                    if (j < pow(2,n-2)/2): ##intera ate a metade do vetor delayss
                        delaysS[j]+=delays_layers[i]["tp"]
                        rS[j]*=delays_layers[0]["rp"]
                    else:
                        delaysS[j]+=delays_layers[i]["ts"]
                        rS[j]*=delays_layers[0]["rs"]

    dS={"ts":delaysS,"rs":rS}

    return (dS)



def delayReflections(tp,rp,ts0,tp0,model,n):
    '''
    Calcula o tempo de chegada e amplitude das ondas refletidas
    '''
    ts = (tp - tp0)*1.73
    tpc = (tp - tp0)

#reflexoes na superficie, onda P na crosta inferior
    tPpPs = round2(tp + tp0 + ts0)
    tPsPs = round2(tp + 2*ts0)
    tPsSs = round2(tp -tp0 + 3*ts0)

    rPsPs = -rp 
    rPpPs = rp
    rPsSs = -rp/2

    if (n>2): ##duas camadas mais semi espaco

#reflexoes na superficie, onda S na crosta inferior
        tSpPs = round2(ts + 2*tp0 + ts0)
        tSsPs = round2(ts + tp0 + 2*ts0)
        tSsSs = round2(ts + 3*ts0)

        rSsPs = rp 
        rSpPs = -rp
        rSsSs = -rp/10

#reflexoes na descontinuidade de conrad, que refletem na moho e chegam a superficie
        tPPPs = round2(3*tpc + ts0)
        tPPSs = round2(2*tpc + ts + ts0)
        tPSSs = round2(tpc + 2*ts + ts0)
        tSSSs = round2(3*ts + ts0)

        rPPPs = round2(rp/9)
        rPPSs = round2(rp/4)
        rPSSs = round2(-rp/6)
        rSSSs = round2(rp/6)

#reflexoes na superficie, onda S na crosta inferior
        tSsPpSs = round2(ts + 2*tp0 + 2*ts0)
        tSsSsSs = round2(ts + 0*tp0 + 4*ts0)

        rSsPpSs = round2(rp*3)
        rSsSsSs = round2(-rp*1.5)

# dicionario que contem todos os delays e ampltitudes
        dRef={"tref":[tPpPs,tPsPs,tPsSs,tSpPs,tSsPs,tSsSs,tPPPs,tPPSs,tPSSs,tSSSs,tSsPpSs,tSsSsSs],"rref":[rPpPs,rPsPs,rPsSs,rSpPs,rSsPs,rSsSs,rPPPs,rPPSs,rPSSs,rSSSs,rSsPpSs,rSsSsSs]}

    else: ## camada mais semi espaco

        dRef={"tref":[tPpPs,tPsPs,tPsSs,],"rref":[rPpPs,rPsPs,rPsSs]}
#    print(dRef["tref"], tp, tp0, ts,ts0)
    return (dRef)



def calcArrivals(model,n):

    '''
    Funcao responsavel por orquestrar a chamada das funcoes responsaveis pelo calculo dos delays das ondas P, S e das reflexoes. Recebe o modelo de velocidades e numero de camadas e retorna um dicionario de dicionario com o tempo das chegadas e amplitude de cada fase
    '''

#vetor model (0-h, 1-vp, 2-vs, 3-rho, 4-zp, 5-zs)

#   define uma lista vazia
    delays_layers=[]

#   calcula o tempo de transito para a onda P e S com suas respectivas ampltitudes
    for i in range(n-2,-1,-1):

        tp=round2((model[i][0])/model[i][1])
        ts=round2((model[i][0])/model[i][2])

        rp=round2((2*model[i+1][4])/(model[i][4]+model[i-1][4]))
        rs=round2((2*model[i+1][4])/(model[i][4]+model[i][5]))

#   em cada iteracao adiciona um dicionario a lista, entao se houver duas camadas havera uma lista com dois dicionarios...

        delays_layers.append({"tp":tp,"ts":ts,"rp":rp,"rs":rs})

#   chama as funcoes que fazem o calculo dos tempos de chegadas
#    print(delays_layers)
    dS = timeConversionsS(delays_layers,n)
    dP = tP(delays_layers,n)
    dRef = delayReflections(dP["tp"],dP["rp"],delays_layers[n-2]["ts"],delays_layers[n-2]["tp"],model,n)

##   define um dicionario de dicionario que contem os tempos de chegadas de todas as fases e suas ampltitudes
    dArrivals={"P":dP,"S":dS,"dRef":dRef}
#    print(dArrivals)
    return (dArrivals)



def createRF (data):
    '''
    Funcao responsavel por criar a funcao refletividade (FR ainda nao convolvida com a gaussiana), definindo o dt, tmin, tmax e criar o vetor de dados em si. Recebe o modelo de velociade e retorna a serie temporal
    '''

    #define o passo de tempo
    dx=0.1

    #cria o vetor de tempo crescente de 0 a 40 incremeta de dt 
    x=np.arange(-5,35,dx)

    #cria e zera o vetor de amplitude com o mesmo tamanho do vetor de tempo
    y=np.zeros(len(x))

    #numero de linhas do arquivo
    n=int(len(data)/4)
    data = np.reshape(data,(4,n)).T.tolist()

    #cria o vetor model com duas colunas a mais que o data para adicionar valores de impedacia
    model = np.zeros((n,6))

    #preenche o vetor model (h, vp, vs, rho, zp, zs)
    for i in range(n):
        model[i]=(data[i][0],data[i][1],data[i][2],data[i][3],data[i][1]*data[i][3],data[i][2]*data[i][3])


    delays = calcArrivals(model,n)

    y = createSpikes(x,y,delays)
    tp=delays["P"]["tp"]

#    ts=delays["S"]["ts"] ## add
#    print("ts-tp = ", ts[0]-tp)
#    plt.plot(x,y)
#    plt.plot(x-tp,y,'blue')
#    plt.show()

#   interpola os dados para o vetor base
    yc=np.interp(x,x-tp,y)

#   retorna a FR com o o primeico pico (chegada da P) em x-0
    return (x,yc)




def derivate(data,yreal):
    '''
    Calculo da derivada dos parametros, analise de sensibilidade, covariancia e numero de codicao
    '''
    d=0.1 #  define o incremento da derivada
    derivates=[] #define uma lista vazia

    # define a legenda para o plot das derivadas
    if int(len(data))/4 == 2:
#       data=['dh1','dh2','dvp1','dvp2','dvs1','dvs2','drho1','drho2'] ##disposicao das variaves no vetor
        legend=['dh1','dvp1','dvs1']
        rang=[0,2,4] ## posicao das variaveis de interesse
    else:
#        data=['dh1','dh2','dh3','dvp1','dvp2','dvp3','dvs1','dvs2','dvs3','drho1','drho2','drho3'] ##disposicao das variaves no vetor
        legend=['dh1','dh2','dvp1','dvp2','dvs1','dvs2']
        rang=[0,1,3,4,6,7] ## posicao das variaveis de interesse

# calcula a derivada para todos os parametros e plota a figura
    for i in rang:

        dv=data[i]*d
        data[i]+=dv
        x,ycalc=fwdProblem(data)
        der=(ycalc-yreal)/dv
        derivates.append(der)

#   inicializa a figura
    plt.figure(figsize=(20,10))

# plota os coeficientes de sensibilidade
    for i in range(len(derivates)):
        plt.plot(x,derivates[i],label=legend[i])

    plt.legend()
    plt.show()


# cria um vetor com as derivadas
    aux=[]
    for i in range(len(derivates)):
        aux.append(derivates[i])

# transforma de lista para um np array
    cf=np.array(aux) 

# matriz de covariacia
    mc=np.cov(cf) 

# calcula o desvio padrao dos parametros
    desvpad=np.sqrt(np.diag(mc)) 

# transformacao USV svd
    U,S,V = np.linalg.svd(mc, full_matrices=True) 

# exibe no terminal os valores encontrados e a matriz de covariacia

    print("\n\n Matriz de Covariancia")
    print(mc)
    print("\nDesvio padrao"+str(desvpad))
    print("\nNumero de condicao "+str(round2(np.max(S)/np.min(S))))

    return 0

def putNoise(y,factor):
    '''
    Adiciona ruido aos dados
    '''
    noise=factor*(np.random.uniform(size=y.shape)-0.5)*np.max(np.abs(y))
    return (y+noise)


def readReal(filename):
    '''
    Le uma FR de um arquivo SAC e interpola os dados 
    '''
    #define o passo de tempo
    dx=0.1

    #cria o vetor de tempo crescente de 0 a 40 incremeta de dt 
    x=np.arange(-5,35,dx)

    #le o traco
    trace = io.load(filename, format='sac')
    #seleciona somente a serie temporal
    tr = trace[0]
    yreal=normalize(np.interp(x,tr.get_xdata()-tr.max()[0],tr.get_ydata()))
#    plt.plot(x,yreal)
#    plt.show()

    return (x,yreal)


def saveBestModel(result,i):
    '''
    Salva no disco o melhor modelo encontrado para ajustar os dados. Cada modelo encontrado e  salvo na pasta inv com o nome inv_modelX, onde x e o numero da inversao.
    '''
    n=int(len(result)/4)
    aux=np.reshape(result,(4,n)).T
    np.savetxt('inv/inv_model'+str(i),aux)
    print("Model save as: inv/inv_model"+str(i))

    return 0


def saveFwdm(x,y,filename):
    '''
    recebe a serie tempora e o nome do arquivo para salva-la
    '''

    np.savetxt(filename, np.transpose([x,y]))
    print("FR salva em:"+filename)

    return 0

def calMeanSTD(stname,path, filename):

    values=[]
    pat=[]

    for i in os.listdir(path):
        aux = np.loadtxt(path+"/"+i)
        values.append(aux[0])
        pat.append(aux[1])

    val = np.array(values).T

    valMean=[]
    valSTD=[]

    for i in range(len(val)):
        valMean.append(round3(val[i].mean()))
        valSTD.append(round3(val[i].std()))

    with open(filename,"a") as f:
        print(stname,valMean[0],valSTD[0],valMean[1],valSTD[1],valMean[2],valSTD[2],file=f)

    with open("end_model","w") as g:
        for i in valMean:
            print(i, end=' ',file=g)
        print("",file=g)
        for j in pat[0]:
            print(round3(j),end= " ",file=g)

    print(stname,valMean[0],"±",valSTD[0],valMean[1],"±",valSTD[1])
 
    return 0



def fwdProblem(data):
    '''
    Funcao para orquestrar a construcao do modelo direto. Chama a funcao que cria a funcao refletividade de acordo com o modelo dado, cria a gaussiana e a convolve com a funcao refletividade gerando a FR em si.
    '''

    ##fwd problem
    x,y = createRF(data)
    xg = np.linspace(-1, 1, 50)
    gauss=gaussian(xg,30,0)
    ycalc = normalize(ss.convolve(y,gauss, mode="same") / (sum(gauss)*5))
#    plt.plot(x,ycalc)
#    plt.show()

    return (x,ycalc)


def fobj(data):
    '''
    Funcao de minimizacao. Busca pelo menor residuo entre a FR real e a calculada.
    '''
#   fr real
    global yreal

#   fr calculada
    x,ycalc = fwdProblem(data)

    res = yreal-ycalc
    Qs = np.linalg.norm(res,ord=2)

#   mostra no terminal o residuo
#    print(Qs)
    
    return Qs

def fobjPSO(data):
    '''
    Funcao de minimizacao. Busca pelo menor residuo entre a FR real e a calculada.
    '''
    npart=data.shape[0]
    Qslist=[]

#   fr real
    global yreal


    for i in range(npart):

    #   fr calculada
        x,ycalc = fwdProblem(data[i])
#        plt.plot(x,yreal,'r')
#        plt.plot(x,ycalc,'b')
#        plt.show()

        res = yreal-ycalc
        Qs = np.linalg.norm(res,ord=2)

    #   mostra no terminal o residuo
        Qslist.append(Qs)

    return (np.array(Qslist))

def invProblemPSO(n):
    '''
    Funcao do problema inverso.Recebe apenas o numero de camadas do modelo a ser minimizado, e com essa informacao define os limites de busca de cada variavel. No caso desse estudo somente as velocidades P, S e a profundidade sao incognitas. As demais estao sendo consideradas como conhecidas para viabilizar a inversao.
    '''

    npart=100 #n particulas
    options = {'c1':1.55, 'c2':1.90, 'w':0.5}  
# c1 experiencia cognitiva, c2 experiencia social

#   definicao dos limites de busca (boundaries)

    if (n==2):
        dimensao=8
        lb=[33,0.00,6.0,8.00,3.47,4.70,2.65,3.30]
        ub=[48,0.01,7.0,8.01,4.05,4.71,2.66,3.31]

    elif (n==3):
        dimensao=12
        lb=[13,15,0.0,5.2,6.2,8.0,3.0,3.50,4.7,2.50,2.65,3.3]
        ub=[20,27,0.1,6.2,7.2,8.1,3.6,4.16,4.8,2.65,2.75,3.4]

    else:
        print("Ate o momento este programa so funciona para duas e tres camadas")

#   chamada da inversao em si, pelo metodo de busca global differential_evolution

    optimizer = pso.single.GlobalBestPSO(n_particles=npart,
                                    dimensions=dimensao,
                                    options=options,
                                    bounds=(np.array(lb),np.array(ub)))

    cost, result = optimizer.optimize(fobjPSO, print_step=5, iters=20, verbose=3)

    return (result)


def invProblemDE(n):
    '''
    Funcao do problema inverso.Recebe apenas o numero de camadas do modelo a ser minimizado, e com essa informacao define os limites de busca de cada variavel. No caso desse estudo somente as velocidades P, S e a profundidade sao incognitas. As demais estao sendo consideradas como conhecidas para viabilizar a inversao.
    '''

#   definicao dos limites de busca (boundaries)

    if (n==2):
        bnds = ((33,48),(0,0),(6.3,6.8),(8.0,8.0),(3.6,3.9),(4.7,4.7),(2.65,2.65),(3.3,3.3))
    elif (n==3):
        bnds = ((13,20),(15,27),(0,0),(5.2,6.2),(6.2,7.2),(8.0,8.0),(3.0,3.6),(3.5,4.16),(4.7,4.7),(2.5,2.5),(2.75,2.75),(3.3,3.3))
    else:
        print("Ate o momento este programa so funciona para duas e tres camadas")

#   chamada da inversao em si, pelo metodo de busca global differential_evolution

    result = differential_evolution(fobj,bnds,tol=0.001)
    print(result)

    return (result.x)


def main():
    '''
    define os menus de funcionalidade do programa e chama as funcoes correnspondentes, de acordo com a escolha do usuario.
    '''
    global yreal
    argc = len(sys.argv)

# se houver argumentos salva o primeiro deles como a opcao, caso nao tenha, a funcao de ajuda e exibida e o programa se encerra

    if (argc > 1):
        option = sys.argv[1]
    else:
        help()
        return 0

############################ Modelo direto ############################

    if (option == '-fwd' and int(argc)== 5):

        print("Calculando a FR")
        filein = sys.argv[2] #modelo de camadas e velocidades
        fileout = sys.argv[3] #nome do aquivo de saida
        ruido = float(sys.argv[4]) #valor do ruido a ser aplicado nos dados

        data = iniModel(filein)
        x,y = fwdProblem(data)

        if (ruido > 0.001): # avalia se sera adicionado ruido ao problema ou nao
            ycalc=putNoise(y,ruido)
        else:
            ycalc=y

        saveFwdm(x,ycalc,fileout) # salva no disco

########### Calcula dos coeficientes de sensibilidade ##################

    elif (option == '-sens' and int(argc)==3):
    
        print("Calculando os coeficientes de sensibilidade")
        filein = sys.argv[2] #modelo de camadas e velocidades

        data = iniModel(filein)
        x,ycalc = fwdProblem(data)
        derivate(data,ycalc)

######################### Faz a inversao ################################

    elif (option == '-inv' and int(argc)==5):

        print("Realizando a inversao")
        filein = sys.argv[2] #arqivo com a FR a ser invertida (serie temporal)
        n = int(sys.argv[3]) #numero de camadas (contando com semi espaco)
        ninv = int(sys.argv[4]) # numero de inversoes

#   verifica se o aquivo lido esta no formato sac, se estiver chama a funcao para ler sac

        if (filein.split(".")[-1] == "sac"):
            xreal,yreal = readReal(filein)
        else:
            xreal, yreal = np.loadtxt(filein,unpack=True)

#   apaga os modelos salvos em inversoes anteriores

        os.system("rm inv/inv*") 

#   faz a inversao n vezes e salva o modelo encontrado em arquivos individuais

        for i in range(1,ninv+1):
            print("\nInversao numero "+str(i))
            result = invProblemPSO(n)
            saveBestModel(result,i)

#   calcula os valores medios e desvio padrao

        st=filein.split(".")[0].split("/")[-1]

        calMeanSTD(st,"inv","table")

################# Plota figuras para se avaliar a inversao ###############

    elif (option == '-plot' and int(argc)==7):

        print("Gerando as comparacoes")
        initFR = sys.argv[2] #serie temporal, FR
        modelreal = sys.argv[3] #modelo inicial
        modelinv = sys.argv[4] #modelo final
        modelmean = sys.argv[5] #modelo final
        ninv = int(sys.argv[6]) # numero de inversoes

# carrega a FR

        if (initFR.split(".")[-1] == "sac"):
            x,yreal = readReal(initFR)
        else:
            x,yreal = np.loadtxt(initFR,unpack=True)


# calcula a FR para o modelo invertido
        res=0
        for i in range(1,ninv+1):
            data2 = iniModel("inv/"+modelinv+str(i))
            x2,ycalc = fwdProblem(data2)
            res += np.linalg.norm(yreal-ycalc,ord=2)

# plota a comparacao da fr real e calculada
#            print(x-x2)

            plot2(x,yreal,ycalc,i,ninv,res,modelreal) 

# compara o modelo de velocidade real e calculado
        for i in range(1,ninv+1):
            plot(modelreal,"inv/"+modelinv+str(i),modelmean,i,ninv,modelreal) 

    else:
        help()


main()


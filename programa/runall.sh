#!/bin/bash

#zera arquivo de dados
> table

for i in RFfiles/*sac
do 
    echo "Working on $i"
    st=`echo $i | awk -F"[/.]" '{print $2}'`
    line=`cat sta_final.txt | grep $st | awk '{print $4,6.4,$5,2.65}'`

    if [ -n "$line" ]; then 
        sed -i "1s/.*./$line/" ini_model
    else
        sed -i "1s/.*./0/" ini_model
    fi

    ./fr.py -inv $i 2 20
    ./fr.py -plot $i ini_model inv_model end_model 20
done

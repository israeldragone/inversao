P=0.10

for CALP in 05 10 20 25 50
do
case ${CALP} in
	05) ALP=0.5;;
	10) ALP=1.0;;
	20) ALP=2.0;;
	25) ALP=2.5;;
	50) ALP=5.0;;
esac

hrftn96 -P -ALP ${ALP} -DT 0.1 -D 10. -RAYP ${P} -M model.true -2
mv hrftn96.sac ${CALP}.rfn
done
